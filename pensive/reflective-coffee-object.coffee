{coffeeFunction} = require './coffee-function'

x = (methods) ->
  o = (methods) -> arguments.callee.extend(methods)
  for n,f of this
    o[n] = f
  for n,c of methods
    o[n] = if c and (c instanceof Function) then c else coffeeFunction c
  o

exports.Reflective = (x.extend = x).extend
  getDullSource: () ->
    ["class\n"].concat(
       for x,y of this when (c = y.coffeeSource)?
         "  " + x + ":\n" + (c.replace /^/g, "    ") + "\n"
    ).join("")
  getSource: ->
    [
      """
      (require "reflective-coffee-object").Reflective
      """
    ].concat(
       for n,f of this when (c = f.coffeeSource)?
         "  " + n + ": \"" + (c.replace /"/g, "\\\"") + "\"" # "
    ).concat([""]).join("\n")
