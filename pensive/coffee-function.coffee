{eval: coffeeEval} = require 'coffee-script'

exports.coffeeFunction =
  (x) -> do (r = coffeeEval x) ->
    r.coffeeSource = x
    r
