{Reflective} = require "reflective-coffee-object"

SelfPrinting = Reflective
  selfPrintScript:
    "-> console.log @getSource() + \".selfPrintScript()\""

SelfPrinting.selfPrintScript()
